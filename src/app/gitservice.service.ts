import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable()
export class GitserviceService {

  constructor(private _http: BaseService) { }

  // End points
  private _getAllUsersUrl: string = "users";
  private _getAllReposUrl: string = "repos";
  private _getSearchUserUrl: string = "search";



  // fetch all users
  getAllUsers() {
    let url = this._getAllUsersUrl;
    return this._http.get(url);
  }

  // fetch all repos
  getRepos(data) {
    let url = this._getAllUsersUrl + "/" + data + "/" + this._getAllReposUrl;
    return this._http.get(url);
  }

  //search for user by name
  searchUsers(data) {
    let url = this._getSearchUserUrl + "/" + this._getAllUsersUrl;
    return this._http.get(url, { q: data });
  }

}
