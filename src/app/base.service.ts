import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BaseService {

  constructor(private _httpClient: HttpClient) { }

  // base url
  public _base_url: string = "https://api.github.com/";

  get(url, queryParams = {}) {
    return this._httpClient.get(this._base_url + url, { params: queryParams });
  }
}

