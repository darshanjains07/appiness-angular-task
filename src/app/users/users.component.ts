import { Component, OnInit } from '@angular/core';
import { GitserviceService } from '../gitservice.service';
import { Router } from '@angular/router';

export interface User {
  login: string;
  avatar_url: string;
  html_url: string;
  repos_url: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private _gitService: GitserviceService, private _router: Router) { }

  public displayedColumns: string[] = ['avatar', 'name', 'repos', 'profile'];
  public users: User[] = [];

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this._gitService.getAllUsers().subscribe((result: any) => {
      if (result.length > 0) {
        this.users = result;
      }
    }, (err: any) => {
      console.log(err);
    });
  }

  navigateToRepos(userName: string) {
    this._router.navigate(['/repos/', userName]);
  }

  searchUser(event) {
    let query = event.target.value;
    if (query == '') {
      this.getUsers();
      return;
    }
    this._gitService.searchUsers(query).subscribe((result: any) => {
      this.users = result.items;
    }, (err: any) => { console.log(err); });
  }
}
