import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GitserviceService } from '../gitservice.service';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.css']
})
export class ReposComponent implements OnInit {

  public userName = '';
  public repos: any[] = [];

  constructor(private _activatedRoute: ActivatedRoute,
    private _gitService: GitserviceService) { }

  ngOnInit() {
    this._activatedRoute.params.forEach((params: Params) => {
      this.userName = params['userName'];
      this._gitService.getRepos(this.userName).subscribe((result: any) => {
        this.repos = result;
      },
        (err: any) => { })
    });
  }

}
